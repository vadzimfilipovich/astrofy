<div align="center" style="font-weight: bold; font-size: 28px;">Astrofy Shopping Platform</div>
<div align="center" >
    <img src="./docs/images/chip.png" style="margin: 20px; width: 300px"/>
</div>
<div align="center" style="flex: 1; flex-direction: row;">
    <img src="./docs/images/react.png" style="margin: 20px; height: 100px"/>
    <img src="./docs/images/redux.png" style="margin: 20px; height: 100px"/>
    <img src="./docs/images/firebase.png" style="margin: 20px; height: 100px"/>
    <img src="./docs/images/nodejs.png" style="margin: 20px; height: 100px"/>
    <img src="./docs/images/graphql.png" style="margin: 20px; height: 100px"/>
    <img src="./docs/images/postgres.svg" style="margin: 20px; height: 100px"/>
</div>

<div align="center" style="font-weight: bold; font-size: 20px;">
Astrofy - TypeScript-powered online store project with crossplatform mobile app and server side, based on React Native and Express.JS. 
</div>

### Documentation
Check latest documentation in the <code>docs</code> folder.
Read [Project SRS](./docs/SRS.md) to establish compatibility.

### Start the project
    //Clone the project
    git clone https://vadzimfilipovich@bitbucket.org/vadzimfilipovich/astrofy.git
    cd astrofy

    //To run client
    cd client && yarn
    //To run iOS client
    yarn ios
    //To run Android client
    yarn android

    //To run server
    cd server && yarn && yarn start
